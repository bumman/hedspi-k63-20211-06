package views.admin.station;

import views.abstracts.EditTableStationPane;
import controller.admin.station.UpdateStationController;
import models.bean.Station;

import javax.swing.*;

public class UpdateStation extends EditTableStationPane {

    private JButton btnBack;
    private JButton btnUpdate;

    public JButton getBtnBack() {
        return btnBack;
    }

    public JButton getBtnUpdate() {
        return btnUpdate;
    }

    public void init(Station station) {
        super.init();

        UpdateStationController updateStationController = new UpdateStationController(this);

        tfID.setEditable(false);
        tfID.setText(Integer.toString(station.getStationID()));

        tfName.setText(station.getStationName());
        tfAddress.setText(station.getStationAddress());
        tfTotal_dock.setText(Integer.toString(station.getNoDock()));

        btnBack = new javax.swing.JButton();
        btnBack.setText("Back");
        btnBack.setBounds(100, 580, 140, 40);
        updateStationController.btnBack();

        btnUpdate = new javax.swing.JButton();
        btnUpdate.setText("Update");
        btnUpdate.setBounds(280, 580, 140, 40);
        updateStationController.btnUpdate(station);

        stationInfoPane.add(btnBack);
        stationInfoPane.add(btnUpdate);
    }
}
