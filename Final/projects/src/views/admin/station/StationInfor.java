package views.admin.station;

import views.abstracts.StationPane;
import controller.admin.station.StationInforController;
import models.bean.Station;

import javax.swing.*;

public class StationInfor extends StationPane {

    private JButton btnBack = new JButton();

    public JButton getBtnBack() {
        return btnBack;
    }

    public void setBtnBack(JButton btnBack) {
        this.btnBack = btnBack;
    }

    public StationInfor() {
    }

    public void init(Station station) {
        super.init(station);
        StationInforController stationInforController = new StationInforController(this);

        btnBack.setBounds(100, 580, 140, 40);
        btnBack.setText("Back");
        stationInfor.add(btnBack);
        stationInforController.btnBack();
    }

    public JPanel getStationInfor() {
        return stationInfor;
    }
    public void setStationInfor(JPanel stationInfor) {
        this.stationInfor = stationInfor;
    }
}
