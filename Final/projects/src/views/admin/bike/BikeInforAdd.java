package views.admin.bike;

import views.abstracts.AnEditableBikePane;
import controller.admin.bike.AdminAddBikeController;
import models.database.SQLDatabase;

import javax.swing.*;

public class BikeInforAdd extends AnEditableBikePane {

    private JButton btnBack;
    private JButton btnAdd;

    public JButton getBtnBack() {
        return btnBack;
    }

    public JButton getBtnAdd() {
        return btnAdd;
    }

    public BikeInforAdd() {
    }

    public void init() {
        super.init();

        AdminAddBikeController adminAddBikeController = new AdminAddBikeController(this);
        tfID.setEditable(false);
        tfID.setText(Integer.toString(SQLDatabase.GetInstance().getNewBikeID()));

        btnBack = new javax.swing.JButton();
        btnBack.setText("Back");
        btnBack.setBounds(100, 580, 140, 40);
        adminAddBikeController.btnBack();

        btnAdd = new javax.swing.JButton();
        btnAdd.setText("Add");
        btnAdd.setBounds(280, 580, 140, 40);
        adminAddBikeController.btnAdd();

        bikeInfoPane.add(btnBack);
        bikeInfoPane.add(btnAdd);
    }

}
