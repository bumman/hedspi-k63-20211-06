package views.admin.bike;

import views.abstracts.ABikePane;
import controller.admin.bike.AdminBikeController;
import controller.admin.bike.BikeInforController;
import models.bean.Bike;

import javax.swing.*;

public class BikeInfor extends ABikePane {

    private BikeInforController bikeInforController;
    private JButton btnBack = new JButton();
    private JButton btnViewBike = new JButton();

    public BikeInfor(String stationID) {
        super(stationID);
    }

    public BikeInfor() {
    }

    public JButton getBtnBack() {
        return btnBack;
    }

    public void setBtnBack(JButton btnBack) {
        this.btnBack = btnBack;
    }

    public JButton getBtnViewBike() {
        return btnViewBike;
    }

    public void setBtnViewBike(JButton btnViewBike) {
        this.btnViewBike = btnViewBike;
    }

    public void init(Bike bike, AdminBikeController adminBikeController) {
        super.init(bike);
        bikeInforController = new BikeInforController(this);
        btnBack.setText("Back");
        btnBack.setBounds(100, 580, 140, 40);
        bikeInfor.add(btnBack);
        bikeInforController.btnBack(adminBikeController);
    }

    public JPanel getBikeInfor() {
        return bikeInfor;
    }

    public void setStationInfor(JPanel stationInfor) {
        this.bikeInfor = stationInfor;
    }
}
