package views.admin.bike;

import views.abstracts.ListPane;
import controller.admin.bike.AdminBikeController;
import views.user.bike.detail.BikeTableData;

import javax.swing.*;

public class AdminBike extends ListPane {

    private JButton logout = new JButton();
    private JButton deleteBike = new JButton();
    private JButton btnEdit = new JButton();
    private JButton btnAdd = new JButton();

    public JButton getLogout() {
        return logout;
    }

    public JButton getDeleteBike() {
        return deleteBike;
    }

    public JButton getBtnEdit() {
        return btnEdit;
    }

    public JButton getBtnAdd() {
        return btnAdd;
    }

    private String[] colTable = new String[]{"ID","Name", "Type", "License Plate","Station ID", "Producer"};

    public AdminBike(String title) {
        super(title);
    }

    public void init() {
        super.init();

        AdminBikeController adminBikeController = new AdminBikeController(this);

        BikeTableData tableData = new BikeTableData(colTable, table);
        listPane.add(tableData.getjPanel5());
        adminBikeController.getDataTable(table);

        table.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (evt.getClickCount() == 2) {
                    AdminBikeController.viewDetailBike(table);
                }
            }
        });

        logout.setText("Log out");
        logout.setBounds(40, 605, 100, 40);
        adminBikeController.btnLogOut();

        deleteBike.setText("Delete");
        deleteBike.setBounds(160, 605, 100, 40);
        adminBikeController.btnDelete();
        listPane.add(deleteBike);

        btnEdit.setText("Edit");
        btnEdit.setBounds(280, 605, 100, 40);
        adminBikeController.btnEdit();
        listPane.add(btnEdit);


        btnAdd.setText("Add New");
        btnAdd.setBounds(400, 605, 100, 40);
        adminBikeController.btnAdd();
        listPane.add(btnAdd);

        listPane.add(logout);
    }
}
