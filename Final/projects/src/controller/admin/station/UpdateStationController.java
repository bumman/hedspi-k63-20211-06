package controller.admin.station;

import views.admin.station.UpdateStation;
import models.bean.Station;
import models.database.SQLDatabase;
import login.AdminPane;
import controller.MainController;

import javax.swing.*;

public class UpdateStationController {

    private UpdateStation updateStation;

    public UpdateStationController(UpdateStation updateStation) {
        this.updateStation = updateStation;
    }

    public UpdateStationController() {
    }

    public void btnBack () {
        updateStation.getBtnBack().addActionListener(e -> {
            AdminPane adminPane = new AdminPane();
            adminPane.init();
            adminPane.setShowIndexTabPanel(1);
            MainController.GetInstance().navigate(adminPane.getPane());
        });
    }

    private boolean updateStation (Station station) {
        return SQLDatabase.GetInstance().updateStation(station);
    }

    public void btnUpdate (Station station) {
        updateStation.getBtnUpdate().addActionListener(e -> {

            String name = updateStation.getTfName().getText();
            String address = updateStation.getTfAddress().getText();
            String dock = updateStation.getTfTotal_dock().getText();


            if (!checkInputStation(name, address, dock)) {
                JOptionPane.showMessageDialog(null, "Input Error!", "error",JOptionPane.ERROR_MESSAGE);
            } else {
                Station newStation = updateStation.returnStation();
                if (!uniqueStationName(station.getStationName(), newStation.getStationName())) {
                    JOptionPane.showMessageDialog(null, "Station name exists!", "error", JOptionPane.ERROR_MESSAGE);
                } else {
                    boolean check = updateStation(newStation);
                    if (!check) {
                        JOptionPane.showMessageDialog(null, "Fail to update!", "error", JOptionPane.ERROR_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(null, "Success to update!");
                    }
                }
            }

            AdminPane adminPane = new AdminPane();
            adminPane.init();
            adminPane.setShowIndexTabPanel(1);
            MainController.GetInstance().navigate(adminPane.getPane());

        });
    }

    public boolean checkInputStation (String nameStation, String nameAddress, String dockStation) {

        String name =nameStation.replaceAll("\\s", "");
        String address = nameAddress.replaceAll("\\s", "");
        String dock = dockStation.replaceAll("\\s", "");

        if (name.length() == 0 || address.length() == 0 || dock.length() == 0) {
            return false;
        } else {
            try {
                Integer.parseInt(dock);
            } catch (Exception e) {
                return false;
            }

            return true;
        }
    }

    public Boolean uniqueStationName (String oldname, String newName) {
        return !(!oldname.equals(newName) && !SQLDatabase.GetInstance().ifStationNameExisted(newName));
    }

}
