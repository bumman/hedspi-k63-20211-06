package controller.admin.bike;

import views.admin.bike.BikeInforUpdate;
import models.bean.Bike;
import models.database.SQLDatabase;
import login.AdminPane;
import controller.MainController;

import javax.swing.*;

public class AdminUpdateBikeController {

    private BikeInforUpdate bikeInforUpdate;
    private AdminBikeInputValidator validator = new AdminBikeInputValidator();

    public AdminUpdateBikeController (BikeInforUpdate bikeInforUpdate) {
        this.bikeInforUpdate = bikeInforUpdate;
    }

    public void btnBack () {
        bikeInforUpdate.getBtnBack().addActionListener(evt -> {
            AdminPane adminPane = new AdminPane();
            adminPane.init();
            adminPane.switchToBikeTab();
            MainController.GetInstance().navigate(adminPane.getPane());
        });
    }

    public boolean btnUpdate(Bike bike) {
            Boolean updateSuccess = true;

            try {
                Bike updatedBike = bikeInforUpdate.returnBike();

                if (validator.ifNameNotUniqueAfterEdit( bike.getBikeName(), updatedBike.getBikeName())) {
                    JOptionPane.showMessageDialog(bikeInforUpdate.getBikeInfoPane(), "Bike with same name already existed", "Error", JOptionPane.ERROR_MESSAGE);
                    updateSuccess = false;
                }

                if (validator.ifLicenseNotUniqueAfterEdit(bike.getLicensePlate(), updatedBike.getLicensePlate())) {
                    JOptionPane.showMessageDialog(bikeInforUpdate.getBikeInfoPane(), "Bike with same license plate already existed", "Error", JOptionPane.ERROR_MESSAGE);
                    updateSuccess = false;
                }

                if (updateSuccess) {
                    SQLDatabase.GetInstance().updateBike(updatedBike);
                    getBackToAdminScreen();
                }

            } catch (NullPointerException | NumberFormatException e) {
                updateSuccess = false;
                JOptionPane.showMessageDialog(bikeInforUpdate.getBikeInfoPane(), "Invalid input, try again!", "Can't update bike", JOptionPane.ERROR_MESSAGE);
            }

            return updateSuccess;
    }

    public void getBackToAdminScreen() {
        AdminPane adminPane = new AdminPane();
        adminPane.init();
        adminPane.switchToBikeTab();
        MainController.GetInstance().navigate(adminPane.getPane());
    }
}
