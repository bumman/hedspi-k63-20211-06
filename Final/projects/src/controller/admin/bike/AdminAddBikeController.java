package controller.admin.bike;

import views.admin.bike.BikeInforAdd;
import models.bean.Bike;
import models.database.SQLDatabase;
import login.AdminPane;
import controller.MainController;

import javax.swing.*;

public class AdminAddBikeController {

    private BikeInforAdd bikeInforAdd;
    private AdminBikeInputValidator validator = new AdminBikeInputValidator();

    public AdminAddBikeController(BikeInforAdd bikeInforAdd) {
        this.bikeInforAdd = bikeInforAdd;
    }

    public void btnBack () {
        bikeInforAdd.getBtnBack().addActionListener(e -> {
            AdminPane adminPane = new AdminPane();
            adminPane.init();
            adminPane.switchToBikeTab();
            MainController.GetInstance().navigate(adminPane.getPane());
        });
    }

    public void btnAdd () {
        bikeInforAdd.getBtnAdd().addActionListener(evt -> {
            Boolean addSuccess = true;

            try {
                Bike newBike = bikeInforAdd.returnBike();
                if (validator.ifNewNameUnique(newBike.getBikeName())) {
                    JOptionPane.showMessageDialog(bikeInforAdd.getBikeInfoPane(), "Bike with same name already existed", "Error", JOptionPane.ERROR_MESSAGE);
                    addSuccess = false;
                }
                if (validator.ifNewLicenseUnique(newBike.getLicensePlate())) {
                    JOptionPane.showMessageDialog(bikeInforAdd.getBikeInfoPane(), "Bike with same license plate already existed", "Error", JOptionPane.ERROR_MESSAGE);
                    addSuccess = false;
                }

                if (addSuccess) {
                    SQLDatabase.GetInstance().addBike(newBike);
                    getBackToAdminScreen();
                }
            } catch (NullPointerException | NumberFormatException e) {
                JOptionPane.showMessageDialog(bikeInforAdd.getBikeInfoPane(), "Invalid input, try again!", "Error", JOptionPane.ERROR_MESSAGE);
            }
        });
    }
    public void getBackToAdminScreen() {
        AdminPane adminPane = new AdminPane();
        adminPane.init();
        adminPane.switchToBikeTab();
        MainController.GetInstance().navigate(adminPane.getPane());
    }
}
