package controller.admin.bike;

import views.admin.bike.BikeInfor;
import controller.MainController;
import views.user.rent.RentBike;

public class BikeInforController {

    private BikeInfor bikeInfor;

    public BikeInforController(BikeInfor bikeInfor) {
        this.bikeInfor = bikeInfor;
    }

    public void btnBack(AdminBikeController bikeController) {
        bikeInfor.getBtnBack().addActionListener(e -> {
            bikeController.getBackToAdminScreen();
        });
    }

    public void rentBikeScreen(int bikeID) {
        bikeInfor.getBtnViewBike().addActionListener(e -> {
            RentBike rentBike = new RentBike(bikeID);
            rentBike.init();
            MainController.GetInstance().navigate(rentBike.getStationInfor());

        });
    }

}
