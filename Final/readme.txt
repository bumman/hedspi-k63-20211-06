		PROJECT ECO BIKE

Nhóm 6

Thành viên:
	Phạm Hoàng Anh    - 20184037
	Tống Ngọc Anh     - 20184040
	Nguyễn Thị Ngọc Diễm - 20184068
	Hoàng Trung Phong - 20184171
Assignments:
   *Phạm Hoàng Anh:
	-Use case: CRUD bãi xe
	-Thiết kế, lập trình và kiểm thử cho usecase trên
	-Merge code với phần của Hoàng Trung Phong để hoàn thành chức năng của người quản trị
	-Thiết kế database
   *Tống Ngọc Anh
   	-Use case: Thanh toán
   	-Thiết kế, lập trình và kiểm thử cho usecase trên
	-Merge code với phần của Nguyễn Thị Ngọc Diễm để hoàn thành chức năng của người dùng
	-Thiết kế package diagram của nhóm
   *Nguyễn Thị Ngọc Diễm:
   	-Use case: Thuê xe
   	-Thiết kế, lập trình và kiểm thử cho usecase trên
	-Lồng ghép với phần của Tống Ngọc Anh để hoàn thành chức năng của người dùng
	-Làm báo cáo 
   *Hoàng Trung Phong:
   	-Use case: CRUD xe
   	-Thiết kế, lập trình và kiểm thử cho usecase trên
	-Hoàn thiện phần package diagram của nhóm
	-Lồng ghép với phần của Phạm Hoàng Anh để hoàn thành chức năng của người quản trị
   		  	
