package ecobike.admin.bike;

import ecobike.admin.dockstation.StationAdd;
import ecobike.bean.Bike;
import ecobike.database.SQLDatabase;
import ecobike.login.AdminPane;
import ecobike.main.MainController;

import javax.swing.*;

public class AdminUpdateBikeController {

    private BikeInforUpdate bikeInforUpdate;
    private AdminBikeInputValidator validator = new AdminBikeInputValidator();

    public AdminUpdateBikeController (BikeInforUpdate bikeInforUpdate) {
        this.bikeInforUpdate = bikeInforUpdate;
    }

    public void click_btnBack () {
        bikeInforUpdate.getBtnBack().addActionListener(evt -> {
            AdminPane adminPane = new AdminPane();
            adminPane.init();
            adminPane.switchToBikeTab();
            MainController.GetInstance().navigate(adminPane.getPane());
        });
    }

    public boolean click_btnUpdate(Bike bike) {
            Boolean updateSuccess = true;

            try {
                Bike updatedBike = bikeInforUpdate.returnBike();

                if (validator.ifNameNotUniqueAfterEdit( bike.getBikeName(), updatedBike.getBikeName())) {
                    JOptionPane.showMessageDialog(bikeInforUpdate.getBikeInfoPane(), "Bike with same name already existed", "Error", JOptionPane.ERROR_MESSAGE);
                    updateSuccess = false;
                }

                if (validator.ifLicenseNotUniqueAfterEdit(bike.getLicensePlate(), updatedBike.getLicensePlate())) {
                    JOptionPane.showMessageDialog(bikeInforUpdate.getBikeInfoPane(), "Bike with same license plate already existed", "Error", JOptionPane.ERROR_MESSAGE);
                    updateSuccess = false;
                }

                if (updateSuccess) {
                    SQLDatabase.GetInstance().updateBike(updatedBike);
                    JOptionPane.showMessageDialog(StationAdd.pane, "Update successfully");
                    getBackToAdminScreen();
                }

            } catch (NullPointerException | NumberFormatException e) {
                updateSuccess = false;
                JOptionPane.showMessageDialog(bikeInforUpdate.getBikeInfoPane(), "Invalid input, try again!", "Can't update bike", JOptionPane.ERROR_MESSAGE);
            }

            return updateSuccess;
    }

    public void getBackToAdminScreen() {
        AdminPane adminPane = new AdminPane();
        adminPane.init();
        adminPane.switchToBikeTab();
        MainController.GetInstance().navigate(adminPane.getPane());
    }
}

