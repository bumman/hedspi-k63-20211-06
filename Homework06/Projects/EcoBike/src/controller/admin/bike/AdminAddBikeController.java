package ecobike.admin.bike;

import ecobike.admin.dockstation.StationAdd;
import ecobike.bean.Bike;
import ecobike.database.SQLDatabase;
import ecobike.login.AdminPane;
import ecobike.main.MainController;

import javax.swing.*;

public class AdminAddBikeController {

    private BikeInforAdd bikeInforAdd;
    private AdminBikeInputValidator validator = new AdminBikeInputValidator();

    public AdminAddBikeController(BikeInforAdd bikeInforAdd) {
        this.bikeInforAdd = bikeInforAdd;
    }

    public void click_btnBack () {
        bikeInforAdd.getBtnBack().addActionListener(e -> {
            AdminPane adminPane = new AdminPane();
            adminPane.init();
            adminPane.switchToBikeTab();
            MainController.GetInstance().navigate(adminPane.getPane());
        });
    }

    public void click_btnAdd () {
        bikeInforAdd.getBtnAdd().addActionListener(evt -> {
            Boolean addSuccess = true;

            try {
                Bike newBike = bikeInforAdd.returnBike();
                if (validator.ifNewNameUnique(newBike.getBikeName())) {
                    JOptionPane.showMessageDialog(bikeInforAdd.getBikeInfoPane(), "Bike with same name already existed", "Error", JOptionPane.ERROR_MESSAGE);
                    addSuccess = false;
                }
                if (validator.ifNewLicenseUnique(newBike.getLicensePlate())) {
                    JOptionPane.showMessageDialog(bikeInforAdd.getBikeInfoPane(), "Bike with same license plate already existed", "Error", JOptionPane.ERROR_MESSAGE);
                    addSuccess = false;
                }

                if (addSuccess) {
                    SQLDatabase.GetInstance().addBike(newBike);
                    JOptionPane.showMessageDialog(StationAdd.pane, "Add new bike successfully");
                    getBackToAdminScreen();
                }
            } catch (NullPointerException | NumberFormatException e) {
                JOptionPane.showMessageDialog(bikeInforAdd.getBikeInfoPane(), "Invalid input, try again!", "Error", JOptionPane.ERROR_MESSAGE);
            }
        });
    }
    public void getBackToAdminScreen() {
        AdminPane adminPane = new AdminPane();
        adminPane.init();
        adminPane.switchToBikeTab();
        MainController.GetInstance().navigate(adminPane.getPane());
    }
}

