package ecobike.admin.bike;


import ecobike.bean.Bike;
import ecobike.database.SQLDatabase;
import ecobike.login.AdminPane;
import ecobike.login.HomePanel;
import ecobike.main.MainController;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;

public class AdminBikeController {

    private AdminBike bikePane;
    public BikeInforAdd bikeAddPane;
    public BikeInforUpdate bikeUpdatePane;

    private AdminBikeInputValidator validator;

    public AdminBikeController(String title) {
        super();

        bikePane = new AdminBike(title);
    }

    public AdminBikeController(AdminBike adminBike) {
        this.bikePane = adminBike;
        bikePane.setListPaneController(this);

        validator = new AdminBikeInputValidator();
    }


    public AdminBike getBikePane() {
        return bikePane;
    }

    public JTable getTable() {
        return bikePane.getTable();
    }

    @Override
    public void getDataTable(JTable table) {
        String query = "SELECT * FROM bikes";
        ArrayList<Bike> bikes = SQLDatabase.GetInstance().getAllBike(query);

        DefaultTableModel model = (DefaultTableModel) table.getModel();
        model.setRowCount(0);
        Object[] row = new Object[6];
        for (Bike bike : bikes) {
            row[0] = bike.getBikeID();
            row[1] = bike.getBikeName();
            row[2] = bike.getClass().getSimpleName();
            row[3] = bike.getLicensePlate();
            row[4] = bike.getStationID();
            row[5] = bike.getProducer();

            model.addRow(row);
        }
    }




    public void logoutAdminScreen() {
        bikePane.getLogout().addActionListener(e -> {
            HomePanel loginScreen = new HomePanel();
            loginScreen.init();
            MainController.GetInstance().navigate(loginScreen.homescreen);
        });
    }


    public void deteteBike () {
        bikePane.getDeleteBike().addActionListener(e -> {
            if (!bikePane.getTable().getSelectionModel().isSelectionEmpty()) {
                int selectedRow = bikePane.getTable().getSelectedRow();
                int selectedBikeID = (int) bikePane.getTable().getValueAt(selectedRow, 0);
                if (validator.ifBikeDeletionInvalid(selectedBikeID)) {
                    JOptionPane.showMessageDialog(null, "Cannot delete, this bike is currently rented!", "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    SQLDatabase.GetInstance().deleteBike(selectedBikeID);
                    JOptionPane.showMessageDialog(bikePane.getListPane(), "Success!", "Message", JOptionPane.INFORMATION_MESSAGE);
                    this.getDataTable(bikePane.getTable());
                }
            }
        });
    }


    public void updateBike() {
        bikePane.getBtnEdit().addActionListener(e -> {
            if (!bikePane.getTable().getSelectionModel().isSelectionEmpty()) {
                int selectedRow = bikePane.getTable().getSelectedRow();
                int bikeId = Integer.parseInt(bikePane.getTable().getValueAt(selectedRow, 0).toString());

                Bike selectedBike = SQLDatabase.GetInstance().getBikeById(bikeId);

                bikeUpdatePane = new BikeInforUpdate();
                bikeUpdatePane.setBikeController(this);
                bikeUpdatePane.init(selectedBike);
                MainController.GetInstance().navigate(bikeUpdatePane.getBikeInfoPane());
            }
        });
    }

    public void addNewBike() {
       bikePane.getBtnAdd().addActionListener(e -> {
           bikeAddPane = new BikeInforAdd();
           bikeAddPane.setBikeController(this);
           bikeAddPane.init();
           MainController.GetInstance().navigate(bikeAddPane.getBikeInfoPane());
       });
    }
    

    public void viewDetailBike (JTable table) {
        AdminBikeController bikeController = new AdminBikeController("bike");
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        int selected = table.getSelectedRow();
        Bike bike = new SQLDatabase().getABike(model.getValueAt(selected, 0).toString());

        BikeInfor bikeInfor = new BikeInfor();
        bikeInfor.init(bike,bikeController);
        MainController.GetInstance().navigate(bikeInfor.getBikeInfor());

    }


}

