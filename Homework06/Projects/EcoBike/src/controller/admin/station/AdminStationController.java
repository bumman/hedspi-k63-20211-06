package controller.admin.station;

import views.admin.station.AddStation;
import views.admin.station.AdminStation;
import views.admin.station.StationInfor;
import views.admin.station.UpdateStation;
import models.bean.Station;
import models.database.SQLDatabase;
import login.AdminPane;
import login.HomePanel;
import controller.MainController;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;

public class AdminStationController   {


    private AdminStation adminStation;

    public AdminStationController(AdminStation adminStation) {
        this.adminStation = adminStation;
    }


    public void getDataTable(JTable table, String query) {
        ArrayList<Station> station = SQLDatabase.GetInstance().getStation(query);
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        model.setRowCount(0);
        Object[] row = new Object[4];
        for (Station value : station) {
            row[0] = value.getStationID();
            row[1] = value.getStationName();
            row[2] = value.getStationAddress();
            row[3] = value.getNoDock();
            model.addRow(row);
        }
    }

    public void createStationAddPage () {
        AddStation addPane = new AddStation();
        addPane.init();
        MainController.GetInstance().navigate(addPane.getStationInfoPane());
    }



    public boolean deleteStation(int stationID) {

        if (getEmptyDock(stationID) != 0) {
            int countNoTwinBike = SQLDatabase.GetInstance().countBikeInStation("TwinBike", Integer.toString(stationID));
            int countNoEcoBike = SQLDatabase.GetInstance().countBikeInStation("EcoBike", Integer.toString(stationID));
            int countNoBike = SQLDatabase.GetInstance().countBikeInStation("NormalBike", Integer.toString(stationID));

            if ((countNoBike != 0) || (countNoEcoBike != 0) || (countNoTwinBike != 0)) {
                return false;
            } else {
                SQLDatabase.GetInstance().deleteStation(stationID);
                AdminPane adminPane = new AdminPane();
                adminPane.init();
                adminPane.setShowIndexTabPanel(1);
                MainController.GetInstance().navigate(adminPane.getPane());
                return true;
            }
        } else {
            SQLDatabase.GetInstance().deleteStation(stationID);
            AdminPane adminPane = new AdminPane();
            adminPane.init();
            adminPane.setShowIndexTabPanel(1);
            MainController.GetInstance().navigate(adminPane.getPane());
            return true;
        }
    }

    public int getEmptyDock (int stationID) {
        return SQLDatabase.GetInstance().getAStation(String.valueOf(stationID)).getNoDock();
    }

    public void viewDetailStation (Station station) {
        StationInfor stationInfor1 = new StationInfor();
        stationInfor1.init(station);
        MainController.GetInstance().navigate(stationInfor1.getStationInfor());
    }

    public void btnAddStation() {
        adminStation.getBtnAdd().addActionListener(e -> createStationAddPage());
    }

    public void btnDeleteStation() {
        adminStation.getDeleteStation().addActionListener(e -> {
        if (!adminStation.getTable().getSelectionModel().isSelectionEmpty()) {
            int selectedRow = adminStation.getTable().getSelectedRow();
            int selectedStationID = (int) adminStation.getTable().getValueAt(selectedRow, 0);
            if (deleteStation(selectedStationID)) {
                JOptionPane.showMessageDialog(null, "Success to delete!");
            } else {
                JOptionPane.showMessageDialog(null, "Delete fail!","error", JOptionPane.ERROR_MESSAGE);
            }
            } else {
                JOptionPane.showMessageDialog(null, "Please chose station!","error", JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    public void createUpdateStationPage (int stationID) {
        Station selectedStation = SQLDatabase.GetInstance().getAStation(Integer.toString(stationID));
        UpdateStation updatePane = new UpdateStation();
        updatePane.init(selectedStation);
        MainController.GetInstance().navigate(updatePane.getStationInfoPane());
    }

    public void btnEditStation() {
        adminStation.getBtnEdit().addActionListener(e -> {
            if (!adminStation.getTable().getSelectionModel().isSelectionEmpty()) {
                int selectedRow = adminStation.getTable().getSelectedRow();
                int stationId = Integer.parseInt(adminStation.getTable().getValueAt(selectedRow, 0).toString());

                createUpdateStationPage(stationId);
            } else {
                JOptionPane.showMessageDialog(null, "Please chose station!","error", JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    public void btnLogOut () {
        adminStation.getLogout().addActionListener(e -> {
            HomePanel loginScreen = new HomePanel();
            loginScreen.init();
            MainController.GetInstance().navigate(loginScreen.homescreen);
        });
    }

    public void tableViewDetailStation () {
        adminStation.getTable().addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (evt.getClickCount() == 2) {
                    DefaultTableModel modelStation = (DefaultTableModel) adminStation.getTable().getModel();
                    int selectRowStation = adminStation.getTable().getSelectedRow();
                    Station station = new SQLDatabase().getAStation(modelStation.getValueAt(selectRowStation, 0).toString());
                    viewDetailStation(station);
                }
            }
        });
    }

}
