package controller.admin.station;

import views.admin.station.AddStation;
import models.bean.Station;
import models.database.SQLDatabase;
import login.AdminPane;
import controller.MainController;


import javax.swing.*;

public class AddStationController {

    private AddStation addStation;

    public AddStationController() {

    }

    public AddStationController(AddStation addStation) {
        this.addStation = addStation;
    }

    public void btnBack () {
        addStation.getBtnBack().addActionListener(e -> {
            AdminPane adminPane = new AdminPane();
            adminPane.init();
            adminPane.setShowIndexTabPanel(1);
            MainController.GetInstance().navigate(adminPane.getPane());
        });
    }

    public void btnAdd () {
        addStation.getBtnAdd().addActionListener(evt -> {
            String name = addStation.getTfName().getText();
            String address = addStation.getTfAddress().getText();
            String dock = addStation.getTfTotal_dock().getText();

            if (!checkInputStation(name, address, dock)) {
                JOptionPane.showMessageDialog(null, "Input error!","error",JOptionPane.ERROR_MESSAGE);
            } else {

                Station station = addStation.returnStation();
                if (!uniqueStationName(station.getStationName())) {
                    JOptionPane.showMessageDialog(null, "Station name exists!", "error", JOptionPane.ERROR_MESSAGE);
                } else {
                    boolean check = addStation(station);
                    if (!check) {
                        JOptionPane.showMessageDialog(null, "Fail to add!");
                    } else {
                        JOptionPane.showMessageDialog(null, "Success to add!");
                    }
                }
            }

            AdminPane adminPane = new AdminPane();
            adminPane.init();
            adminPane.setShowIndexTabPanel(1);
            MainController.GetInstance().navigate(adminPane.getPane());
        });
    }

    public boolean checkInputStation (String nameStation, String nameAddress, String dockStation) {

        String name =nameStation.replaceAll("\\s", "");
        String address = nameAddress.replaceAll("\\s", "");
        String dock = dockStation.replaceAll("\\s", "");

        if (name.length() == 0 || address.length() == 0 || dock.length() == 0) {
            return false;
        } else {
            try {
                Integer.parseInt(dock);
            } catch (Exception e) {
                return false;
            }

            return true;
        }
    }

    public Boolean uniqueStationName (String newName) {
        return (SQLDatabase.GetInstance().ifStationNameExisted(newName));
    }

    private boolean addStation (Station station) {
        return SQLDatabase.GetInstance().addStation(station);
    }

}
