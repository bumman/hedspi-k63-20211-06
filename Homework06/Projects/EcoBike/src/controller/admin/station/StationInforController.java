package controller.admin.station;

import views.admin.station.StationInfor;
import login.AdminPane;
import controller.MainController;



public class StationInforController {

    private StationInfor stationInfor;

    public StationInforController(StationInfor stationInfor) {
        this.stationInfor = stationInfor;
    }

    public void btnBack() {
        stationInfor.getBtnBack().addActionListener(e -> {
            AdminPane adminPane = new AdminPane();
            adminPane.init();
            adminPane.setShowIndexTabPanel(1);
            MainController.GetInstance().navigate(adminPane.getPane());
        });
    }
}
