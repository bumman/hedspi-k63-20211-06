package ecobike.admin.bike;

import ecobike.abstracts.AnEditableBikePane;
import ecobike.database.SQLDatabase;

import javax.swing.*;

public class BikeInforAdd extends AnEditableBikePane {

    private JButton btnBack;
    private JButton btnAdd;

    public JButton getBtnBack() {
        return btnBack;
    }

    public JButton getBtnAdd() {
        return btnAdd;
    }

    public BikeInforAdd() {
    }

    public void init() {
        super.init();

        AdminAddBikeController adminAddBikeController = new AdminAddBikeController(this);
        tfID.setEditable(false);
        tfID.setText(Integer.toString(SQLDatabase.GetInstance().getNewBikeID()));

        btnBack = new javax.swing.JButton();
        btnBack.setText("Back");
        btnBack.setBounds(100, 580, 140, 40);
        adminAddBikeController.click_btnBack();

        btnAdd = new javax.swing.JButton();
        btnAdd.setText("Add");
        btnAdd.setBounds(280, 580, 140, 40);
        adminAddBikeController.click_btnAdd();

        bikeInfoPane.add(btnBack);
        bikeInfoPane.add(btnAdd);
    }

}

