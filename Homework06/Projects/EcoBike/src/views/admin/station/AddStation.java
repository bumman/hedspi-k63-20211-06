package views.admin.station;

import views.abstracts.EditTableStationPane;
import controller.admin.station.AddStationController;

import javax.swing.*;

public class AddStation extends EditTableStationPane {

    public static JPanel pane;

    private JButton btnBack;
    private JButton btnAdd;

    public JButton getBtnBack() {
        return btnBack;
    }

    public JButton getBtnAdd() {
        return btnAdd;
    }

    public void init() {
        super.init();

        AddStationController addStationController = new AddStationController(this);

//        tfID.setEditable(false);
        tfID.setText("0");
        tfID.setVisible(false);
        lbID.setVisible(false);

        btnBack = new javax.swing.JButton();
        btnBack.setText("Back");
        btnBack.setBounds(100, 580, 140, 40);
        addStationController.btnBack();

        btnAdd = new javax.swing.JButton();
        btnAdd.setText("Add");
        btnAdd.setBounds(280, 580, 140, 40);

        addStationController.btnAdd();
        stationInfoPane.add(btnBack);
        stationInfoPane.add(btnAdd);
    }

}
