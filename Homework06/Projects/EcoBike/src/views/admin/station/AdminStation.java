package views.admin.station;

import controller.admin.station.AdminStationController;
import views.user.station.list.StationTableData;
import views.abstracts.*;

import javax.swing.*;

public class AdminStation extends ListPane {

    private JButton logout = new JButton();
    private JButton deleteStation = new JButton();
    private JButton btnEdit = new JButton();
    private JButton btnAdd = new JButton();

    public JButton getLogout() {
        return logout;
    }

    public JButton getDeleteStation() {
        return deleteStation;
    }

    public JButton getBtnEdit() {
        return btnEdit;
    }

    public JButton getBtnAdd() {
        return btnAdd;
    }

    private String[] colTable = new String[]{"ID", "Name", "Address", "Total_dock"};

    public AdminStation(String title) {
        super(title);
    }



    public void init() {
        super.init();

        AdminStationController adminStationController = new AdminStationController(this);

        String query = "select * from stations";
        StationTableData tableData = new StationTableData(colTable, table);
        listPane.add(tableData.getjPanel5());
        adminStationController.getDataTable(table ,query);

        adminStationController.tableViewDetailStation();

        logout.setText("Log out");
        logout.setBounds(40, 605, 100, 40);
        adminStationController.btnLogOut();
        listPane.add(logout);

        deleteStation.setText("Delete");
        deleteStation.setBounds(160, 605, 100, 40);
        adminStationController.btnDeleteStation();
        listPane.add(deleteStation);

        btnEdit.setText("Edit");
        btnEdit.setBounds(280, 605, 100, 40);
        adminStationController.btnEditStation();
        listPane.add(btnEdit);

        btnAdd.setText("Add");
        btnAdd.setBounds(400, 605, 100, 40);
        adminStationController.btnAddStation();
        listPane.add(btnAdd);
    }
}
