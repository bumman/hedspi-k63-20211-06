package views.admin.station;

import controller.admin.station.*;
import org.junit.Test;

import static org.junit.Assert.*;


public class testUpdateStationController {
    UpdateStationController updateStationController = new UpdateStationController();
//    AdminStation adminStation = new AdminStation("station");

    @Test
    public void testUniqueNameStation () {

        String newName = "qqqqwwewewe";
        String oldName = "Station-1";
        assertEquals(true, updateStationController.uniqueStationName(oldName, newName));

        newName = "Station-2";
        oldName = "Station-1";
        assertEquals(false, updateStationController.uniqueStationName(oldName, newName));
    }

    @Test
    public void testInputStation () {
        String name = "   ";
        String address = "Ha Noi";
        String dock = "3";

        assertFalse(updateStationController.checkInputStation(name, address, dock));

        String name1 = "Station-7";
        String address1 = "  ";
        String dock1 = "3";

        assertFalse(updateStationController.checkInputStation(name1, address1, dock1));

        String name2 = "Station-7";
        String address2 = "Ha Noi";
        String dock2 = "a";

        assertFalse(updateStationController.checkInputStation(name2, address2, dock2));

        name2 = "Station-7";
        address2 = "Ha Noi";
        dock2 = "6";

        assertTrue(updateStationController.checkInputStation(name2, address2, dock2));
    }
}
