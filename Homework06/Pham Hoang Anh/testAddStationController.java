package views.admin.station;

import controller.admin.station.AddStationController;
import org.junit.Test;

import static org.junit.Assert.*;


public class testAddStationController {
    AddStationController addStationController = new AddStationController();

    @Test
    public void testUniqueNameStation () {

        String uniqueName1 = "qqqqwwewewe";
        assertEquals(true, addStationController.uniqueStationName(uniqueName1));

        String uniqueName2 = "Station-1";
        assertEquals(false, addStationController.uniqueStationName(uniqueName2));
    }

    @Test
    public void testInputStation () {
        String name = "   ";
        String address = "Ha Noi";
        String dock = "3";

        assertFalse(addStationController.checkInputStation(name, address, dock));

        String name1 = "Station-7";
        String address1 = "  ";
        String dock1 = "3";

        assertFalse(addStationController.checkInputStation(name1, address1, dock1));

        String name2 = "Station-7";
        String address2 = "Ha Noi";
        String dock2 = "a";

        assertFalse(addStationController.checkInputStation(name2, address2, dock2));

        name2 = "Station-7";
        address2 = "Ha Noi";
        dock2 = "6";

        assertTrue(addStationController.checkInputStation(name2, address2, dock2));
    }
}
