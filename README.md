                            PROJECT ECOBIKE

* Group number: 6

* Members:
    Phạm Hoàng Anh		- 201840374
    Hoàng Trung Phong		- 20184171
    Tống Ngọc Anh		- 20184040
    Nguyễn Thị Ngọc Diễm	- 20184068

* Assignments:
    * Phạm Hoàng Anh: 25%
        - Use case: Add Station, Update Station.
        - Design, develop and test logic and UI for above use case. 
        - Thiết kế, phát triển, chức năng và kiểm thử cho use case mình phụ trách.
        - Quản lý mã nguồn.
    * Tống Ngọc Anh: 25% 
   	    - Use case: Thanh toán.
   	    - Thiết kế, lập trình và kiểm thử cho usecase trên.
	    - Merge code với phần của Nguyễn Thị Ngọc Diễm để hoàn thành chức năng của người dùng
	    - Thiết kế package diagram của nhóm
    * Nguyễn Thị Ngọc Diễm: 25%
   	    - Use case: Thuê xe
   	    - Thiết kế, lập trình và kiểm thử cho usecase trên
	    - Lồng ghép với phần của Tống Ngọc Anh để hoàn thành chức năng của người dùng
	    - Làm báo cáo 
    * Hoàng Trung Phong: 25%
   	    - Use case: CRUD xe
   	    - Thiết kế, lập trình và kiểm thử cho usecase trên
	    - Hoàn thiện phần class diagram của nhóm
	    - Lồng ghép với phần của Phạm Hoàng Anh để hoàn thành chức năng của người quản trị

* Run:
    * Sử dụng database mysql.
    * Thêm tất cả các file (.jar) trong thư mục Plugin.
    * Run: file Main.java